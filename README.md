# `widgets`

This is the `widgets` FSL React component library. Most widgets build on the [React MUI](https://mui.com) library. These components are intended to be used in the FSL React web-based [GUIs](https://git.fmrib.ox.ac.uk/fsl/gui).

# Development

## Prerequisites

It is assumed that your development machine is either a Mac or Linux machine.

- [nodejs and NPM](https://nodejs.org/en/)

## Setup

Clone and cd into this repo

```bash
git clone https://git.fmrib.ox.ac.uk/fsl/gui/widgets.git

cd widgets
```

Install dependencies via npm

```bash
npm install
```

## Building

To build the library, run the following command:

```bash
npm run build
```

**Important**: when building, some packages are externalised. This means that they are not bundled with the library. Instead, they are expected to be provided by the consuming application (but they are also listed as dependencies of this library, so should get installed via `npm install`). The following packages are externalised: `react, @mui/material, @mui/icons-material, @emotion/react`

## Publishing

Publishing is a manual process until the proper CI/CD pipeline is in place. To publish, run the following command:

```bash
npm run pub
```

