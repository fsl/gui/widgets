import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Snackbar from '@mui/material/Snackbar';
import { useState } from 'react';

/**
 * A preview of the command that will be run
 * @param {string} commandString - the command string to display
 * @param {object} props - other props
 * @returns {ReactElement} - the command preview
 * @example
 * <CommandPreview commandString={commandString} />
 */
export function CommandPreview({ commandString, multiLine=false, ...props }){
    // the open state of the snackbar
    const [open, setOpen] = useState(false)
    // copy text to clipboard when clicked
    function onClick(){
        navigator.clipboard.writeText(commandString)
        setOpen(true)
    }

    return (
        <Box
        sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            textAlign: multiLine ? 'left' : 'center',
            justifyContent: 'left',
            width: '100%',
            gap: 2
        }}
        >
        <Typography 
            fontFamily={'monospace'} 
            variant="body"
            onClick={onClick}
            sx={{
                whiteSpace: multiLine ? 'wrap' : 'nowrap',
            }}
        >
            {commandString}
        </Typography>
        {/* A snackbar is a short-lived pop up message that auto-hides after a set duration */}
        <Snackbar
            open={open}
            autoHideDuration={3000}
            onClose={() => setOpen(false)}
            message="Copied to clipboard"
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        />
        </Box>
    )
}