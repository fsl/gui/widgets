import Box from '@mui/material/Box';

/**
 * A container component that displays its children in a Flexbox column.
 * @param {Object} props - The component props.
 * @param {ReactNode} props.children - The child elements to display.
 * @example
 * <Column>
 * <div>Child 1</div>
 * <div>Child 2</div>
 * </Column>
 */
export function Column({ children, ...props }){
    return (
        <Box
        sx={{
            display: 'flex',
            flexDirection: 'column',
            ...props
        }}
        >
        {children}
        </Box>
    )
}