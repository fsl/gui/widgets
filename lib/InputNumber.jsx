import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export function InputNumber({ 
    value, 
    textLabel='input', 
    helperLabel='',
    onChange=()=>{}, 
    min, 
    max, 
    step,
    ...props 
  }) {
      
      const handleChange = (event) => {
          let newValue = Number(event.target.value)
          if (newValue < min) newValue = min
          onChange(newValue);
      };
  
      return (
          <Box
          sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
          }}
          >
          <TextField
              fullWidth
              label={textLabel}
              helperText={helperLabel}
              onChange={handleChange}
              value={value}
              size='small'
              inputProps={{
                min: {min},
                max: {max},
                step: {step},
                type: 'number',
              }}
              variant='standard'
          />
          </Box>
      );
      }