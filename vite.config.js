import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    port: 5199
  },
  // make all paths relative
  base: './',
  build: {
    lib: {
        entry: resolve(__dirname, 'lib/widgets.jsx'),
        name: 'widgets',
        fileName: 'widgets',
    },
    rollupOptions: {
        external: ['react', '@mui/material', '@mui/icons-material', '@emotion/react'],
    }
}
})
